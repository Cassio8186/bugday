// Desafio 2 questões
#include <stdio.h>
int main(void)
{
    int i, quant;
    double num, maior, menor;
    printf("\nQuantos você digitar?\n");
    scanf("%i", &quant);
    printf("\nEntre com um número\n");
    scanf("%lf", &num);

    maior = num;
    menor = num;
    for (i = 1; i < quant; ++i)
    {
        printf("\nEntre com um número\n");
        scanf("%lf", &num);
        if (num > maior)
        {
            maior = num;
        }
        else if (num < menor)
        {
            menor = num;
        }
    }
    printf("\nO maior número é: %lf", maior);
    printf("\nO menor número é: %lf \n", menor);

    return 0;
}

