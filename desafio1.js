// primeira questão: fibonacci
var cont, fib, ant, atual;

var cont = 1;
var repeticao = 11;
while (cont < repeticao) {
	if (cont === 1) {
		fib = 1;
		ant = 0;
		atual = 1;
	} else {
		fib = ant + atual;
		ant = atual;
		atual = fib;
	}
	console.log("Posição", cont, "Fibonacci", fib);
	cont = cont + 1;
}
