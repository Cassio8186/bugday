// desafio 3 - javascript
let espaco = "    ";
for (let linha = 1; linha < 10; linha++) {
	let texto = "";
	texto = texto.concat(espaco.repeat(linha - 1));
	for (let coluna = 2; coluna < 10; coluna++) {
		if (linha < coluna) {
			texto = texto.concat(`${linha}-${coluna} `);
		}
	}
	console.log(texto);
}
